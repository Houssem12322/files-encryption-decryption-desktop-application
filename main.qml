import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: page
    visible: true
    width: 640
    height: 480
    title: qsTr("M.H.S.C")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1 {
        }

        Page2 {
        }
    }

    header: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Home")
        }
        TabButton {
            text: qsTr("Seeding")
        }
    }
    Dialog {
        id: errorDialog

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: Math.min(parent.width, parent.height) / 3 * 2
        contentHeight: 60
        parent: ApplicationWindow.overlay

        modal: true
        title: "Message"
        standardButtons: Dialog.Close

        Flickable {
            id: flickable
            clip: true
            anchors.fill: parent
            contentHeight: column.height

            Column {
                id: column
                spacing: 20
                width: parent.width


                Label {
                    id: errorText
                    width: parent.width
                    text: "Message"
                    wrapMode: Label.Wrap
                }
            }

            ScrollIndicator.vertical: ScrollIndicator {
                parent: errorDialog.contentItem
                anchors.top: flickable.top
                anchors.bottom: flickable.bottom
                anchors.right: parent.right
                anchors.rightMargin: -errorDialog.rightPadding + 1
            }
        }

    }
    Connections {
        target: isaac
        onError:{
                errorText.text = msg
                errorDialog.visible = true
        }
        onSeedingDone: {
            errorText.text = msg
            errorDialog.visible = true
            tabBar.currentIndex = 0

        }

    }


}
