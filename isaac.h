#ifndef ISAAC_H
#define ISAAC_H

#include <QObject>
#include <QByteArray>
#include <QFile>
#include <QFileDialog>
#include <QString>

class ISAAC : public QObject
{
    Q_OBJECT
public:
    explicit ISAAC(QObject *parent = nullptr);


signals:
   void error (QString msg);
   void seedingDone(QString msg);

public slots:
    void encrypt();
    void decrypt();
    void seeding();


private:
    uint f(uint a,uint i);
    void isaac();


    QFile _States;
    QByteArray _StatesHistory;
    uint _StateArray[256];
    uint _Result[256];
    uint _A, _B, _Iteration;
    uchar _C;
};

#endif // ISAAC_H
