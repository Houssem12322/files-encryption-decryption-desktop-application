#include "isaac.h"
#include "QDebug"

ISAAC::ISAAC(QObject *parent) : QObject(parent)
{
    _States.setFileName("states.bin");
    if(!_States.open(QIODevice::ReadWrite ))
    {
        emit error(tr("Impossible to read the file 'states.bin'."));
        exit(0);
    }
    _StatesHistory = _States.readAll();
    _States.close();
}

void ISAAC::encrypt(){

    if(_StatesHistory.isEmpty())
    {
        emit error(tr("Please choose the seed and retry."));
        return;
    }
    QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Select File to encrypt"), "/home");
    if(fileName.isNull())
    {
        return;
    }
    QFile fileToEncrypt(fileName);
    if(!fileToEncrypt.open(QIODevice::ReadOnly))
    {
        emit error(tr("Error when opening the target."));
        return;
    }

    QByteArray arrayToEncrypt = fileToEncrypt.readAll();
    if(arrayToEncrypt.mid(0,29) == "martin houssem encrypted file")
    {
        emit error("This file is already encrypted");
        return;
    }

    uint fileIteration = *(uint*)(_StatesHistory.data());
    _A = *(uint*)(_StatesHistory.data()+4);
    _B = *(uint*)(_StatesHistory.data()+8);
    _C = *(uchar*)(_StatesHistory.data()+12);
    uint* temp = (uint*)(_StatesHistory.data()+13);
    for(int j=0; j<256; j++)
    {
        _StateArray[j] = temp[j];
    }

    QByteArray format(10,'\0');
    uint indexOfFormat;
    indexOfFormat = fileName.indexOf(".", fileName.size()-10);
    for(int i=indexOfFormat+1, j=0; i<fileName.size(); i++, j++)
    {
        format[j] = fileName.at(i).toLatin1();
    }

    arrayToEncrypt.push_front(format);
    uint index = 0;
    uint fileSize = arrayToEncrypt.size();
    uchar* resultAsUchar = (uchar*)_Result;
    _Iteration = fileIteration;


    while(index < fileSize)
    {
        isaac();
        _Iteration++;
        for(int i=0; (i<1024 && index<fileSize); i++)
        {
            arrayToEncrypt[index] = arrayToEncrypt.at(index) ^ resultAsUchar[i];
            index++;
        }
    }
    arrayToEncrypt.prepend((char*)(&fileIteration),4);
    arrayToEncrypt.push_front("martin houssem encrypted file");

    fileName.replace(indexOfFormat,fileName.size()-indexOfFormat,".bin");
    QFile encryptedFileToSave(fileName);
    if(!encryptedFileToSave.open(QIODevice::WriteOnly))
    {
        emit error(tr("Error when saving the encrypted file."));
        return;
    }
    encryptedFileToSave.write(arrayToEncrypt);
    _StatesHistory.remove(1037*9,1037);
    _StatesHistory.prepend((char*)&_StateArray, 1024);
    _StatesHistory.prepend((char*)&_C,1);
    _StatesHistory.prepend((char*)&_B,4);
    _StatesHistory.prepend((char*)&_A,4);
    _StatesHistory.prepend((char*)&_Iteration, 4);

    if(!_States.open(QIODevice::WriteOnly ))
    {
        emit error(tr("Impossible to read the file 'states.bin'."));
        exit(0);
    }
    _States.write(_StatesHistory);
    _States.close();

    emit error(tr("Encryption done."));



}

void ISAAC::decrypt(){

    if(_StatesHistory.isEmpty())
    {
        emit error(tr("Please choose the seed and retry."));
        return;
    }
    QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Select File to decrypt"), "/home");
    if(fileName.isNull())
    {
        return;
    }
    QFile fileToDecrypt(fileName);
    if(!fileToDecrypt.open(QIODevice::ReadOnly))
    {
        emit error(tr("Error when opening the target."));
        return;
    }

    QByteArray arrayToDecrypt = fileToDecrypt.readAll();
    if(arrayToDecrypt.mid(0,29) != "martin houssem encrypted file")
    {
        emit error(tr("This file is not encrypted using this application."));
        return;
    }

    uint fileIteration = *(uint*)(arrayToDecrypt.data()+29);
    for(int i=0; i<_StatesHistory.size(); i+=1037)
    {
        _Iteration = *(uint*)(_StatesHistory.data()+i);

        if(fileIteration >= _Iteration)
        {
            _A = *(uint*)(_StatesHistory.data()+i+4);
            _B = *(uint*)(_StatesHistory.data()+i+8);
            _C = *(uchar*)(_StatesHistory.data()+i+12);
            uint* temp = (uint*)(_StatesHistory.data()+i+13);
            for(int j=0; j<256; j++)
            {
                _StateArray[j] = temp[j];
            }
            break;
        }
    }
    for(uint i=_Iteration; i<fileIteration; i++)
    {
        isaac();
    }
    uint index = 33;
    uint fileSize = arrayToDecrypt.size();
    uchar* resultAsUchar = (uchar*)_Result;
    _Iteration = fileIteration;


    while(index < fileSize)
    {
        isaac();
        _Iteration++;
        for(int i=0; (i<1024 && index<fileSize); i++)
        {
            arrayToDecrypt[index] = arrayToDecrypt.at(index) ^ resultAsUchar[i];
            index++;
        }
    }





    fileName.replace(fileName.indexOf('.', fileName.size()-10)+1,10,QString(arrayToDecrypt.mid(33,10)));
    arrayToDecrypt.remove(0,43);

    QFile decryptedFileToSave(fileName);
    if(QFile::exists(fileName))
    {
        fileName.append("a");
    }
    if(!decryptedFileToSave.open(QIODevice::WriteOnly))
    {
        emit error(tr("Error when saving the decrypted file."));
        return;
    }
    _StatesHistory.remove(1037*9,1037);
    _StatesHistory.prepend((char*)&_StateArray, 1024);
    _StatesHistory.prepend((char*)&_C,1);
    _StatesHistory.prepend((char*)&_B,4);
    _StatesHistory.prepend((char*)&_A,4);
    _StatesHistory.prepend((char*)&_Iteration, 4);

    if(!_States.open(QIODevice::WriteOnly ))
    {
        emit error(tr("Impossible to read the file 'states.bin'."));
        exit(0);
    }

    _States.write(_StatesHistory);
    _States.close();
    decryptedFileToSave.write(arrayToDecrypt);
    decryptedFileToSave.close();
    emit error(tr("Decryption done."));



}

void ISAAC::seeding(){

    QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Select File to seed"), "/home");
    if(fileName.isNull())
    {
        return;
    }
    QFile seed(fileName);


    if(!seed.open(QIODevice::ReadOnly))
    {
        emit error("Error when opening the seed file.");
        return;
    }

    QByteArray newState = seed.read(1024);

    if(newState.size()<1024)
    {
        emit error("The seed is not long enough.");
        return;
    }
    uint numIteration = 0;
    uint A = 123123;
    uint B = 123321;
    uchar C = 0;

    newState.push_front(QByteArray((char*)&C,1));
    newState.push_front(QByteArray((char*)&B,4));
    newState.push_front(QByteArray((char*)&A,4));
    newState.push_front(QByteArray((char*)&numIteration,4));



    _StatesHistory.remove(1037*9,1037);
    _StatesHistory.prepend(newState);

    if(!_States.open(QIODevice::WriteOnly ))
    {
        emit error(tr("Impossible to read the file 'states.bin'."));
        exit(0);
    }

    _States.write(_StatesHistory);
    _States.close();
    emit seedingDone("Seeding done.");


}

uint ISAAC::f(uint a, uint i)
{

    switch(i & 3)
    {
        case 0 : return ((a<<13) | (a>>18)); break;
        case 1 : return ((a>>6) | (a<<25)); break;
        case 2 : return ((a<<2) | (a>>29)); break;
        default : return ((a>>16) | (a<<15));

    }
}

void ISAAC::isaac()
{
    uint x;
    _C = _C+1;
    _B = _B+_C;

    for(int j=0; j<256; j++)
    {
        x = _StateArray[j];
        _A = f(_A,j) +_StateArray[(j+128) & 255];
        _StateArray[j] = (_A^_B) + _StateArray[((x<<29)|(x>>2)) & 255];
        _Result[j] = x + (_A^_StateArray[((_StateArray[j]<<21)|(_StateArray[j]>>10)) & 255]);
        _B = _Result[j];

    }


}
