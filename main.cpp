#include <QApplication>
#include <QQmlApplicationEngine>
#include <isaac.h>
#include <QDebug>
#include <QQmlContext>
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    ISAAC isaac;

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("isaac", &isaac);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
