import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    property alias button1: button1

        Button {
            id: button1
            text: qsTr("Choose a key")
            highlighted: true
            width: 200
            height: 60
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                isaac.seeding()
            }
        }

}
