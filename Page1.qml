import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    property alias button1: button1

        ColumnLayout{
            id: layout
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 6
            width: 200

            Button {
                id: button1
                text: qsTr("Encrypt")
                highlighted: true
                Layout.fillWidth: true
                Layout.minimumHeight: 60
                onClicked: {
                    isaac.encrypt()
                }

            }

            Button {
                id: button2
                text: qsTr("Decrypt")
                highlighted: true
                Layout.fillWidth: true
                Layout.minimumHeight: 60
                onClicked:{

                    isaac.decrypt()
                }
            }

        }

}
