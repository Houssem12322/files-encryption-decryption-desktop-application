# Files encryption decryption desktop application

A desktop and smartphone software which encrypt any kind of file using one-time-pad cryptosystem. The later cryptosystem is known to be cryptographically safe. The software is implemented using Qt framework. 

USAGE SCENARIO: suppose you want to transfair files securely and you don't want that any one read your file even it's the service provider it self (email, cloud storage, website, .....).
First you need a key of length 1024 bytes (8192 bits) or bigger (better binary random brut data) between two or as many as you like persons. the key will be stretched using ISSAC PRNG. The encryption and decryption is done using one-time-pade cryptosystem by xoring the output of ISSAC PRNG with the data files.  

REMARK: the binary random brut data can be generated using the code 'https://gitlab.com/Houssem12322/cpp-code-to-generate-random-data-from-camera-noise'.

This project can be build using Qt 5.9 or later version.

For more information feel free to contact me on my email adress 'houssem.merdaci@gmail.com'